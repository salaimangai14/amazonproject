package Test;

import org.testng.annotations.Test;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import Base.TestBase;
import Pages.Signuppage;
import Util.Testutil;

public class Createaccounttest extends TestBase {
	
	Signuppage signup;
	String sheetname="test";
  public Createaccounttest() throws IOException {
		super();
		
	}

@BeforeMethod
public void setup() throws IOException
{
	Initialize();
	signup=new Signuppage();
}

/*@Test(priority=1)
public void clicksignup() throws InterruptedException
{
	signup.signupaction();
	Thread.sleep(2000);
}*/

@Test(priority=2,dataProvider="userdetails")
public void createnewacct(String firstname, String email, String password, String reenterpassword) throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException
{
	System.out.println("hi");
	signup.signupaction();
	signup.createnewaccount(firstname, email, password, reenterpassword);
}

@DataProvider(name="userdetails")
public Object[][] getexceldata() throws EncryptedDocumentException, InvalidFormatException, IOException{
	Object data[][]=Testutil.getTestData(sheetname);
	
	return data;
	
}

@AfterMethod
public void browserclose()
{
	driver.quit();
}

}
