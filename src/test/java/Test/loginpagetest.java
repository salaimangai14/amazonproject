package Test;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.TestBase;
import Pages.Loginpage;

public class loginpagetest extends TestBase {
	Loginpage loginpage;
	public loginpagetest() throws IOException {
		super();

	}

	@BeforeMethod
	public void setup() throws IOException
	{
		Initialize();
		loginpage=new Loginpage();
	}
	
	@Test
	public void login()
	{
		loginpage.clicksignin();
		loginpage.enteremailid(properties.getProperty("username"));
	}
	@AfterMethod
	public void closebrowser()
	{
		driver.quit();
	}
}
