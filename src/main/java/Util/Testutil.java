package Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import Base.TestBase;

public class Testutil extends TestBase {

	public Testutil() throws IOException {
		super();

	}

	public static long pageloadtimeout=20;
	public static long implicitwait=20;

	public static String testdatasheetpath=System.getProperty("user.dir")+"/src/main/java/Testdata/Data.xlsx";
	static Workbook workbook;
	static Sheet sheet;

	public static Object[][] getTestData(String sheetName) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		Object[][] data=null;
		File file=new File(testdatasheetpath);
		FileInputStream fis=new FileInputStream(file);
		XSSFWorkbook workbook=new XSSFWorkbook(fis);
		XSSFSheet sheet=workbook.getSheet(sheetName);
		int rowcount=sheet.getLastRowNum()-sheet.getFirstRowNum();
		System.out.println(sheet.getLastRowNum() +" "+sheet.getFirstRowNum());
		System.out.println(sheet.getRow(0).getLastCellNum());
		 data=new Object[rowcount][sheet.getRow(0).getLastCellNum()];
		for(int i=0;i<rowcount;i++)
		{
			Row row=sheet.getRow(i+1);
			for(int j=0;j<row.getLastCellNum();j++)
			{
				data[i][j]=row.getCell(j).toString();
				System.out.println(data[i][j]);
				
			}
		}
		
		return data;
		

	}

	public static void takescreenshotatend() throws IOException
	{
		TakesScreenshot s=((TakesScreenshot)driver);
		File src=s.getScreenshotAs(OutputType.FILE);
		String currentdir=System.getProperty("user.dir");
		FileUtils.copyFile(src,new File(currentdir+"/Screenshots/"+System.currentTimeMillis()+".png"));
	}
	
	public static void mouseactions(WebElement element)
	{
		
		Actions action=new Actions(driver);
		action.moveToElement(element).perform();
	}

}
