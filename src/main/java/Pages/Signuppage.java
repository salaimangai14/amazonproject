package Pages;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Base.TestBase;
import Util.Testutil;

public class Signuppage extends TestBase{

	@FindBy(xpath="//*[@id='nav-link-accountList']")
	WebElement createaccount;
	
	@FindBy(linkText="Start here.")
	WebElement createaccountlink;
	
	@FindBy(id="ap_customer_name")
	WebElement fname;
	
	@FindBy(id="ap_email")
	WebElement emailvalue;
	
	@FindBy(id="ap_password")
	WebElement passwordvalue;
	
	@FindBy(id="ap_password_check")
	WebElement reenterpasswordvalue;
	
	public Signuppage() throws IOException {
		super();
		PageFactory.initElements(driver, this);
	}
	
	public void signupaction() throws InterruptedException
	{
		Testutil.mouseactions(createaccount);
		Thread.sleep(2000);
		createaccountlink.click();
	}
	
	public void createnewaccount(String firstname,String email,String password,String reenterpassword) throws InterruptedException
	{
		
		Thread.sleep(2000);
		fname.sendKeys(firstname);
		emailvalue.sendKeys(email);
		passwordvalue.sendKeys(password);
		reenterpasswordvalue.sendKeys(reenterpassword);
	}
	
	  
  }

