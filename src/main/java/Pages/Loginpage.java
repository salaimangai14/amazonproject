package Pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import Base.TestBase;


public class Loginpage extends TestBase{
	
	@FindBy(xpath="//*[@id='nav-link-accountList']/span[1]")
	WebElement loginelement;
	@FindBy(name="email")
	WebElement emailtextbox;

	public Loginpage() throws IOException {
		super();
		PageFactory.initElements(driver, this);
	}
	
	public void clicksignin()
	{
		loginelement.click();
	}

	public void enteremailid(String email)
	{
		emailtextbox.sendKeys(email);
		emailtextbox.clear();
		
	}
}
